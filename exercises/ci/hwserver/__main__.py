#!/usr/bin/env python
import asyncio
from aiohttp import web
import asyncpg
import os

conn = None


async def default_handler(request):
    values = await conn.fetch(
        'insert into logs (data) values ($1) returning id',
        request.headers['User-Agent'],
    )
    rid = values[0]["id"]
    data = {
        "msg": "Hello!",
        "id": rid,
        "agent": request.headers['User-Agent']
    }
    print(data)
    return web.json_response(data)


def on_startup():
    async def func(_):
        global conn
        conn = await asyncpg.connect(os.environ['DATABASE_URL'], ssl='require')

    return func


def on_shutdown():
    async def func(_):
        await conn.close()

    return func


def main():
    app = web.Application()
    app.on_startup.append(on_startup())
    app.on_shutdown.append(on_shutdown())
    app.router.add_get('/', default_handler)

    web.run_app(app, host="0.0.0.0", port=int(os.environ['PORT']))


if __name__ == '__main__':
    main()
