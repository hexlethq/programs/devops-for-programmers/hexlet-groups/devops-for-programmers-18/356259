package main

import (
    "fmt"
    "net/http"
    "os"
    "strconv"

    "github.com/jackc/pgx"
)

var conn *pgx.Conn

func handler(w http.ResponseWriter, r *http.Request) {

	data := r.RemoteAddr

	var id int64
	err := conn.QueryRow("insert into logs (data) values ($1) returning id", data).Scan(&id)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		os.Exit(1)
	}

    fmt.Fprintf(w, "Id = %d, Data = %s", id, data)
	fmt.Println(id, data)
}

func getConfig() pgx.ConnConfig {
    var config pgx.ConnConfig;
	config.User = os.Getenv("DBUSER")
	config.Password = os.Getenv("DBPASS")
	config.Database = os.Getenv("DBNAME")
	config.Host = os.Getenv("DBHOST")
	port, err := strconv.Atoi(os.Getenv("DBPORT"))
    if err != nil {
        fmt.Println(err)
        os.Exit(2)
    }
    config.Port = uint16(port)
    fmt.Println(config)
	return config
}

func main() {
    var err error
    conn, err = pgx.Connect(getConfig())
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close()

	http.HandleFunc("/", handler)
    http.ListenAndServe(":8080", nil)
}