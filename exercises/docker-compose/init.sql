begin;

create table if not exists logs(id bigserial, data text);

commit;